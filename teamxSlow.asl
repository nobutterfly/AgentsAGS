// moves left in actual turn
moves(0).
//places(0).

sa(1).

border(X,-1).
border(-1,Y).
border(X,Y) :- grid_size(X,_).
border(X,Y) :- grid_size(_,Y).


possibleRight(MyX,MyY) :- not (known_obstacle(MyX+1, MyY) | border(MyX+1, MyY)).
possibleLeft(MyX,MyY) :- not (known_obstacle(MyX-1, MyY) | border(MyX-1, MyY)).
possibleUp(MyX,MyY) :- not (known_obstacle(MyX, MyY-1) | border(MyX, MyY-1)).
possibleDown(MyX,MyY) :- not (known_obstacle(MyX, MyY+1) | border(MyX, MyY+1)).

distance(A,B,Dist) :- A>B & Dist = A-B.
distance(A,B,Dist) :- A<B & Dist = B-A.
distance(A,B,Dist) :- A=B & Dist = 0.

distance_X(MyX,X,Dist_x) :- distance(MyX,X,Dist_x).
distance_Y(MyY,Y,Dist_y) :- distance(MyY,Y,Dist_y).

left_neigh(HisX, HisY, MyX, MyY) :- HisX == MyX-1 & HisY == MyY. 
right_neigh(HisX, HisY, MyX, MyY) :- HisX == MyX+1 & HisY == MyY. 
up_neigh(HisX, HisY, MyX, MyY) :- HisX == MyX & HisY == MyY-1.
down_neigh(HisX, HisY, MyX, MyY) :- HisX == MyX & HisY == MyY+1.  

reverse([],Z,Z).
reverse([H|T],Z,Acc) :- reverse(T,Z,[H|Acc]).

head([H|T], H).
tail([H|T], HH) :- head(T, HH).

// generate unseen places (foggy places)
+!fillUnseen: grid_size(X,Y)
 <- for (.range (X1,0,X-1)){
		for (.range (Y1,0,Y-1)){
			  +unseen(X1,Y1);
		}}.

// generate the key places to visit to get vision over the whole map
+!fillKeyPlaces: grid_size(Grid_X,Grid_Y)
	<-	+keyPlace(9,10);
		+keyPlace(Grid_X-10,10);
		+keyPlace(Grid_X-10,Grid_Y-10);
		+keyPlace(10,Grid_Y-10);
		+keyPlace(10,10);
		+keyPlace(10,2);
		+keyPlace(Grid_X-3,2);
		+keyPlace(Grid_X-3,Grid_Y-3);
		+keyPlace(2,Grid_Y-3);
		+keyPlace(2,2).
		
// <- for (.range (X1,0,Grid_X-1)){
//		for (.range (Y1,0,Grid_Y-1)){
//			  if (X1 == 3 & Y1 >= 3 & Y1 <= Grid_Y-4) {
//          +keyPlace(X1,Y1);
//        }
//        if (Y1 == 3 & X1 >= 3 & X1 <= Grid_X-4) {
//          +keyPlace(X1,Y1);
//        }
//			  if (X1 == Grid_X-4 & Y1 >= 3 & Y1 <= Grid_Y-4) {
//          +keyPlace(X1,Y1);
//        }
//		}}
//    for (.range (X1,0,Grid_X-1)){
//		for (.range (Y1,0,Grid_Y-1)){
//    	  if (X1 == 9 & Y1 >= 9 & Y1 <= Grid_Y-10) {
//          +keyPlace(X1,Y1);
//        }
//        if (Y1 == 9 & X1 >= 9 & X1 <= Grid_X-10) {
//          +keyPlace(X1,Y1);
//        }
//    }}.

// remove places in sight from unseen places
+!lookAround(X,Y): sa(S)
	<- for(.range(XI,-3*S,3*S)){
			for(.range(YI,-3*S,3*S)){
				-unseen(X+XI,Y+YI);
			}
	   }
     if (keyPlace(X,Y)) {
       -keyPlace(X,Y);
     }.

+!seen_by_friend(X,Y): unseen(X,Y)
	<-	for(.range(XI,-1,1)){
			for(.range(YI,-1,1)){
				-unseen(X+XI,Y+YI);
			}
	    }.

+!seen_by_friend(X,Y).// <- .print("uz som videl, diky").

// find out if aMiddle or bMiddle is my ally
+!choosePicker: friend(aMiddle)
	<- +picker(aMiddle).

+!choosePicker: friend(bMiddle)
	<- +picker(bMiddle).
	

+!set_dest_place([X,Y])
  <- -+destination(X, Y).

// send my actual position to middle agent and actualize
// unseen/keyPlace lists
+pos(X,Y):picker(aMiddle)
	 <- !lookAround(X,Y);
	 	.send(aMiddle, tell, obsPos(X,Y)).

+pos(X,Y):picker(bMiddle)
	 <- !lookAround(X,Y);
	 	.send(bMiddle, tell, obsPos(X,Y)).

// send gold position to middle agent
+gold(X,Y): friend(aMiddle)
	<- //.send(aMiddle, tell, gold(X,Y)).
	.send(aMiddle, achieve, golds(X,Y)).
+gold(X,Y): friend(bMiddle)
	<- //.send(bMiddle, tell, gold(X,Y)).
		.send(bMiddle, achieve, golds(X,Y)).

// send wood position to middle agent	
+wood(X,Y): friend(aMiddle)
	<- //.send(aMiddle, tell, wood(X,Y)).
		.send(aMiddle, achieve, woods(X,Y)).
+wood(X,Y): friend(bMiddle)
	<- //.send(bMiddle, tell, wood(X,Y)).
		.send(bMiddle, achieve, woods(X,Y)).

+spectacles(X,Y): sa(S) & S == 1
	<-	+keyPlace(X,Y).
	
+!spectacless(X,Y): sa(S) & S == 1
	<-	+keyPlace(X,Y).
+!spectacless(X,Y).
	
+shoes(X,Y): friend(aFast)
	<- .send(aFast,achieve,shoess(X,Y)).
	
+shoes(X,Y): friend(bFast)
	<- .send(bFast,achieve,shoess(X,Y)).

//obstacle - moje prekazky
//know_obstacle - moje alebo oznamene prekazky
+!listen_new_obstacle(X,Y) : not known_obstacle(X,Y)
    <- +known_obstacle(X,Y); -unseen(X,Y); -keyPlace(X,Y); 
      // Calculate a new plan only if we are going somewhere and the obstacle is in our way
      if (destination(A,B) & pathRoute(X,Y)) {
        -+new_obstacle(true);
      }.
+!listen_new_obstacle(X,Y).


+!add_known_obstacle(X,Y): not known_obstacle(X,Y) 
      <- +known_obstacle(X,Y); -unseen(X,Y); -keyPlace(X,Y).  
+!add_known_obstacle(X,Y). 


+!find_obstacles
    <- for(obstacle(X,Y)) {
        if (not known_obstacle(X,Y)) {
	        !add_known_obstacle(X,Y);
	        if(friend(aFast)) {
	        	.send(aFast, achieve, listen_new_obstacle(X, Y));
	        	.send(aMiddle, achieve, listen_new_obstacle(X, Y));
	        }
	        else {
	        	.send(bFast, achieve, listen_new_obstacle(X, Y));
	        	.send(bMiddle, achieve, listen_new_obstacle(X, Y));
	        }
          -+new_obstacle(true);
        }
      }.
        
// **************************************************** //



// **************************************************** //
// ------------ Path finding A* functions ------------- //
// **************************************************** //

// Look at elements in Closed and by backtracking from final
// position back to start position create our route (pathRoute).
+!route_from_closed
  <- ?destination(Dst_X, Dst_Y);
     +pathRoute(Dst_X, Dst_Y);
     !route_add_predecessors(Dst_X, Dst_Y);
     ?pos(Cur_X, Cur_Y);
     -pathRoute(Cur_X, Cur_Y).

// Recursive function that goes step by step from final position
// back to start. It uses the last value in Closed to determine
// which way to backtrack.      
+!route_add_predecessors(Child_X, Child_Y)
  <- 
      if (not pos(Child_X, Child_Y) & closed(Child_X, Child_Y, D)) { // If we haven't backtracked to our real start position yet
//        ?closed(Child_X, Child_Y, D);

        if (D == 1) { // D = 1 -> This location was first visited from the right
          +pathRoute(Child_X+1, Child_Y); 
          !route_add_predecessors(Child_X+1, Child_Y); // ...so we backtrack to the right
        }
        if (D == 2) { // This location was first visited from the left
          +pathRoute(Child_X-1, Child_Y);    
          !route_add_predecessors(Child_X-1, Child_Y); // ...so we backtrack to the left
        }
        if (D == 3) { // This location was first visited from the above
          +pathRoute(Child_X, Child_Y-1);
          !route_add_predecessors(Child_X, Child_Y-1);
        }
        if (D == 4) { // This location was first visited from the bellow
          +pathRoute(Child_X, Child_Y+1);
          !route_add_predecessors(Child_X, Child_Y+1); // ...so we backtrack to the bellow
        }
      } 
      // ELSE
      else {     
        .abolish(closed(_,_,_));
      }.


get_route_array(UnseenArray,RouteArray) :- reverse(UnseenArray,RouteArray,[]).

heuristika(MyX,MyY,X,Y,H) :- distance_X(MyX,X,Dist_X) & distance_Y(MyY,Y,Dist_Y) & H = Dist_X+Dist_Y.

//g_number(MyX,MyY,X,Y,G) :- distance_X(MyX,X,Dist_x) & distance_Y(MyY,Y,Dist_y) & H = Dist_X + Dist_Y.

f_number_left(MyX,MyY,X,Y,G,N) :- heuristika(MyX-1,MyY,X,Y,H) & N = H+G.
f_number_right(MyX,MyY,X,Y,G,N) :- heuristika(MyX+1,MyY,X,Y,H) & N = H+G.
f_number_down(MyX,MyY,X,Y,G,N) :- heuristika(MyX,MyY+1,X,Y,H) & N = H+G.
f_number_up(MyX,MyY,X,Y,G,N) :- heuristika(MyX,MyY-1,X,Y,H) & N = H+G.

+!go([X,Y]): spectacles(A,B) & (X == A) & (Y == B) <- .abolish(spectacles(A,B)); !go([X,Y]).

+!go([X,Y]): not spect & spectacles(A,B) & pos(MyX,MyY) & (MyX == A) & (MyY == B)
	<-	-+moves(0);
		.abolish(spectacles(A,B));
		//-pathRoute(X,Y);
		-keyPlace(A,B);
		?sa(S);
		-+sa(S+1);
		do(pick);
		+spect.		
//		}
//		else{
//			-+moves(M-1);-pathRoute(X,Y);
//			do(skip);
//		}.
		
		
+!go([X,Y]): pos(MyX,MyY) & possibleLeft(MyX,MyY) & (MyX > X) & moves(M) & (M>0)
	<-  -+moves(M-1); -pathRoute(X,Y); ?moves(MM); if (MM == 0) {do(left)}.
+!go([X,Y]): pos(MyX,MyY) & possibleRight(MyX,MyY)& (MyX < X) & moves(M) & (M>0)
	<-  -+moves(M-1); -pathRoute(X,Y); ?moves(MM); if (MM == 0) {do(right)}.
+!go([X,Y]): pos(MyX,MyY) & possibleDown(MyX,MyY) & (MyY < Y) & moves(M) & (M>0)
	<-  -+moves(M-1); -pathRoute(X,Y); ?moves(MM); if (MM == 0) {do(down)}.
+!go([X,Y]): pos(MyX,MyY) & possibleUp(MyX,MyY) & (MyY > Y) & moves(M) & (M>0)
	<-  -+moves(M-1); -pathRoute(X,Y); ?moves(MM); if (MM == 0) {do(up)}.
	
+!go([X,Y]): pos(MyX,MyY) & (MyX == X) & (MyY == Y) <- -+moves(M-1); -pathRoute(X,Y); do(skip).
+!go([X,Y]). 

+!find_min_F_coord
  <- for (open(X,Y,_,F,_))
     {
       if (not (minimal_F(_,_,_))) {
         -+minimal_F(F,X,Y);
       }
       else {
         if (minimal_F(MinF,MX,MY)) { //minimal_F(_,_,_)
//          ?minimal_F(MinF,MX,MY);
          if (F < MinF) {
            -+minimal_F(F,X,Y);
          }
        }
       }
     }.

+!fill_open(MyX,MyY,X,Y): (MyX == X) & (MyY == Y) & d(D) // ak prisiel do ciela
	<-	+closed(X,Y,D);
		!route_from_closed.  	
	   	
+!fill_open(MyX,MyY,X,Y)     // X,Y suradnice ciela
	<-	if (possibleLeft(MyX,MyY) & not closed(MyX-1,MyY,_) & not open(MyX-1,MyY,_,_,_)) {    // ak sa da ist dolava
        if (open(MyX,MyY,G,_,_)) {
			    ?f_number_left(MyX,MyY,X,Y,G,FF);  // vypocitaj F = H + G
          +open(MyX-1,MyY,G,FF,1);		     
        }
        else { // This is start position, this location isnt in open
          ?f_number_left(MyX,MyY,X,Y,1,FF);
          +open(MyX-1,MyY,1,FF,1);
        }
	   	} 
	   	if (possibleRight(MyX,MyY) & not closed(MyX+1,MyY,_) & not closed(MyX+1,MyY,_)) {
        if (open(MyX,MyY,G,_,_)) { 
			    ?f_number_right(MyX,MyY,X,Y,G,FR);  // vypocitaj F = H + G
          +open(MyX+1,MyY,G,FR,2);		     
        }
        else { // This is start location neighbour, G=1
          ?f_number_right(MyX,MyY,X,Y,1,FR);
          +open(MyX+1,MyY,1,FR,2);
        }
	   	}
		if (possibleDown(MyX,MyY) & not closed(MyX,MyY+1,_) & not closed(MyX,MyY+1,_)) {
        if (open(MyX,MyY,G,_,_)) { 
			    ?f_number_down(MyX,MyY,X,Y,G,FD);  // vypocitaj F = H + G
          +open(MyX,MyY+1,G,FD,3);		     
        }
        else { // This is start location neighbour, G=1
          ?f_number_down(MyX,MyY,X,Y,1,FD);
          +open(MyX,MyY+1,1,FD,3);
        }
		}
		if (possibleUp(MyX,MyY) & not closed(MyX,MyY-1,_)& not closed(MyX,MyY-1,_)) {
        if (open(MyX,MyY,G,_,_)) { 
			    ?f_number_up(MyX,MyY,X,Y,G,FU);  // vypocitaj F = H + G
          +open(MyX,MyY-1,G,FU,4);		     
        }
        else { // This is start location neighbour, G=1
          ?f_number_up(MyX,MyY,X,Y,1,FU);
          +open(MyX,MyY-1,1,FU,4);
        }
		}

    // If open contains at least one element
    if (open(_,_,_,_,_)) {
      	!find_min_F_coord; //najdi open s minimalnym F
      	//?minimal_F(MinF,MF_X,MF_Y); 
      	//-minimal_F(MinF,MF_X,MF_Y);
		
	  	if (minimal_F(MinF,MF_X,MF_Y)) { //minimal_F(_,_,_)
//			?minimal_F(MinF,MF_X,MF_Y); 
		  	-minimal_F(MinF,MF_X,MF_Y);
		  	if (open(MF_X,MF_Y,G,MinF,D)) {   // Vlez do open			  	
			  	+closed(MF_X,MF_Y,D);				// uloz do closed 
			  	-open(MF_X,MF_Y,G,MinF,D);				// vymaz open
			  	-+d(D);
			  	!fill_open(MF_X,MF_Y,X,Y);			// zavolaj fill open so suradnicami co sa ulozily do closed teda ide sa na dalsie policko
      		}
      	}
	}.

+!calculate_plan([X,Y]): pos(MyX,MyY)
	<- .abolish(pathRoute(_,_)); !fill_open(MyX,MyY,X,Y); .abolish(closed(_,_,_)); .abolish(open(_,_,_,_,_)).

+!visit([F|T]) <- !go(F).
+!visit([F]) <- !go(F). 
+!visit([]): moves(M) & (M>0) <- -+moves(M-1); do(skip).
+!visit([]).

// **************************************************** //
// ------------- Main actions during round ------------ //
// **************************************************** //

//+!help_me(X,Y): friend(aFast)
//	<-	.send(aFast, achieve, help_slow(X,Y)).
//
//+!help_me(X,Y): friend(bFast)
//	<-	.send(bFast, achieve, help_slow(X,Y)).
	
+!visit_place([F|T]) <- 
      !find_obstacles;
      !set_dest_place(F);

      if (destination(Dst_X, Dst_Y) & known_obstacle(Obs_X, Obs_Y) & Dst_X == Obs_X & Dst_Y == Obs_Y) {
        .abolish(pathRoute(_,_));
        !visit([]);
      }
      else {
       // .print("Zacatek visit place");
        if (not pathRoute(_,_) | new_obstacle(_)) {
          -new_obstacle(true);
          !calculate_plan(F);
        }
        // There is no route leading to our goal
        if (not pathRoute(_,_)) {
          ?destination(X,Y);
        //  .print("There is still no plan to ", X, " ",Y);
          -keyPlace(X,Y);
          -unseen(X,Y);
        } else {
          .findall([A,B], pathRoute(A,B), WayToGo);
       //   .print("pathRoute: ", WayToGo);
		      !visit(WayToGo);
        }
      }.  

+!visit_place([F]) <- 
      !find_obstacles;
      !set_dest_place(F);

      ?destination(Dst_X, Dst_Y);
      if (not known_obstacle(Dst_X, Dst_Y)) {
        if (not pathRoute(_,_) | new_obstacle(_)) {
          -new_obstacle(true);
          !calculate_plan(F);
        }
        // There is no route leading to our goal
        if (not pathRoute(_,_)) {
          ?destination(X,Y);
         // .print("There is still no plan to ", X, " ",Y);
          -keyPlace(X,Y);
          -unseen(X,Y);
        } else {
          .findall([A,B], pathRoute(A,B), WayToGo);
         // .print("pathRoute: ", WayToGo);
		      !visit(WayToGo);
        }  
      } else {
        .abolish(pathRoute(_,_));
        -new_obstacle(true);
        !visit([]);
      }.
 


+!visit_place([]): moves(M) & (M>0) <- -+moves(M-1); do(skip).
+!visit_place([]).

// We haven't visited all key places yet, so we go to the next keyPlace
+!turn: moves(M) & (M>0) & keyPlace(_,_)
	<- .findall([X,Y], keyPlace(X,Y), PlacesToGoArray);
	   !visit_place(PlacesToGoArray);
//	   ?unseen(A,B);
//	   !help_me(A,B);
	   !turn.

// We visited all key places but some locations are still unknown, 
// we visit those locations randomly
+!turn: moves(M) & (M>0)
	<- .findall([X,Y], unseen(X,Y), UnseenArray);
	   !visit_place(UnseenArray); 
//	   !help_me(X,Y);
	   !turn.

+!turn.

+step(0) <- !fillUnseen; !fillKeyPlaces; !choosePicker; -+moves(1);!turn.
+step(X) <- -+moves(1);!turn. 

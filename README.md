- Každý agent si vede svou mapu, která je synchronizována s ostatníma agentama pomocí zpráv.
- Bystrozraký hledá zlato. Jakmile ho najde, tak aFast pomůže aMiddle zvednout zlato a ten ho odnese do skladiště. 

Smluvené termíny: 

- 12.3. [Petr]  -> Model funkgující na mapě bez překážek 
- 30.3. [Šimon] -> Agent je schopen se na libovolné prázdné políčko na mapě dostat
                   nehledě na překážky + systematičtější prohledávání neodhalených 
                   políček mapy.  

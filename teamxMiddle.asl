// possible states - following, pickingResource, goToDepot
state(following).

// moves left in actual turn
moves(0).

// count of moves in one turn
maxMoves(2).

border(X,-1).
border(-1,Y).
border(X,Y) :- grid_size(X,_).
border(X,Y) :- grid_size(_,Y).

possibleRight(MyX,MyY) :- not (known_obstacle(MyX+1, MyY) | border(MyX+1, MyY)).
possibleLeft(MyX,MyY) :- not (known_obstacle(MyX-1, MyY) | border(MyX-1, MyY)).
possibleUp(MyX,MyY) :- not (known_obstacle(MyX, MyY-1) | border(MyX, MyY-1)).
possibleDown(MyX,MyY) :- not (known_obstacle(MyX, MyY+1) | border(MyX, MyY+1)).

distance(A,B,Dist) :- A>B & Dist = A-B.
distance(A,B,Dist) :- A<B & Dist = B-A.
distance(A,B,Dist) :- A=B & Dist = 0.

distance_X(MyX,X,Dist_x) :- distance(MyX,X,Dist_x).
distance_Y(MyY,Y,Dist_y) :- distance(MyY,Y,Dist_y).

reverse([],Z,Z).
reverse([H|T],Z,Acc) :- reverse(T,Z,[H|Acc]).

head([H|T], H).
tail([H|T], HH) :- head(T, HH).

+!choosePicker: friend(aFast)
	<- +follower(aFast).

+!choosePicker: friend(bFast)
	<- +follower(bFast).

+!set_dest_place([X,Y])
  <- -+destination(X, Y).

// choose which next resource to pick
// TODO needs to be clever (mby pick closer resource)
+!actGold([X|XS]) <- +actualResource(X).
+!actGold([X]) <- +actualResource(X).

// New gold is discovered
+gold(X,Y): state(following)
	<-  -+state(pickingResource); 
		+actualResource([X,Y]); +goldList(X,Y);
		+resourceType(gold).
		//.abolish(gold(X,Y)).

+gold(X,Y): not goldList(X,Y) <- +goldList(X,Y).//; .abolish(gold(X,Y)).

-gold(X,Y): gold(X,Y) <- .print("dostal som -gold!!!!!!!!!!!!!!") .abolish(gold(X,Y)).

// New wood is discovered
+wood(X,Y): state(following) 
	<-  -+state(pickingResource); 
		+actualResource([X,Y]); +woodList(X,Y);
		+resourceType(wood).
		//.abolish(wood(X,Y)).

+wood(X,Y): not woodList(X,Y) <- +woodList(X,Y).//; .abolish(wood(X,Y)).

//-wood(X,Y): wood(X,Y) <- .abolish(wood(X,Y)).

+!golds(X,Y): state(following)
	<-  -+state(pickingResource); 
		+actualResource([X,Y]); +goldList(X,Y);
		+resourceType(gold). 
		
+!golds(X,Y): not goldList(X,Y) <- +goldList(X,Y).
+!golds(X,Y).

+!woods(X,Y): state(following) 
	<-  -+state(pickingResource); 
		+actualResource([X,Y]); +woodList(X,Y);
		+resourceType(wood).
		
+!woods(X,Y): not woodList(X,Y) <- +woodList(X,Y).
+!woods(X,Y).

+spectacles(X,Y): friend(aSlow)
	<- .send(aSlow,achieve,spectacless(X,Y)).

+spectacles(X,Y): friend(bSlow)
	<- .send(bSlow,achieve,spectacless(X,Y)).

+shoes(X,Y): friend(aFast)
	<- .send(aFast,achieve,shoess(X,Y)).
	
+shoes(X,Y): friend(bFast)
	<- .send(bFast,achieve,shoess(X,Y)).

// slow agent sends me his actual position
+obsPos(X,Y) <- .abolish(obsPos(_,_)); -+goTo(X,Y).

// send my position to fast agent
//+pos(X,Y)
//	 <- ?follower(Fast); .send(Fast, tell, pickerPos(X,Y)).
//+pos(X,Y) : follower(aFast)
//	 <- .send(aFast, tell, pickerPos(X,Y)).

+!listen_new_obstacle(X,Y) : not known_obstacle(X,Y)
    <- +known_obstacle(X,Y);
      // Calculate a new plan only if we are going somewhere and the obstacle is in our way
      if (destination(A,B) & pathRoute(X,Y)) {
        -+new_obstacle(true);
      }.
+!listen_new_obstacle(X,Y).


+!add_known_obstacle(X,Y): not known_obstacle(X,Y) 
      <- +known_obstacle(X,Y).
+!add_known_obstacle(X,Y). 


+!find_obstacles
    <- for(obstacle(X,Y)) {
        if (not known_obstacle(X,Y)) {
	        !add_known_obstacle(X,Y);
	        if(friend(aSlow)) {
	        	.send(aSlow, achieve, listen_new_obstacle(X, Y));
	        	.send(aFast, achieve, listen_new_obstacle(X, Y));
	        }
	        else {
	        	.send(bSlow, achieve, listen_new_obstacle(X, Y));
	        	.send(bFast, achieve, listen_new_obstacle(X, Y));
	        }
	        
          -+new_obstacle(true);
        }
      }.
    
// **************************************************** //


// **************************************************** //
// ------------ Path finding A* functions ------------- //
// **************************************************** //

// Look at elements in Closed and by backtracking from final
// position back to start position create our route (pathRoute).
+!route_from_closed
  <- ?destination(Dst_X, Dst_Y);
     +pathRoute(Dst_X, Dst_Y);
     !route_add_predecessors(Dst_X, Dst_Y);
     ?pos(Cur_X, Cur_Y);
     -pathRoute(Cur_X, Cur_Y).

// Recursive function that goes step by step from final position
// back to start. It uses the last value in Closed to determine
// which way to backtrack.      
+!route_add_predecessors(Child_X, Child_Y) 
  <- 
      if (not (pos(Child_X, Child_Y))) { // If we haven't backtracked to our real start position yet
        ?closed(Child_X, Child_Y, D);

        if (D == 1) { // D = 1 -> This location was first visited from the right
          +pathRoute(Child_X+1, Child_Y); 
          !route_add_predecessors(Child_X+1, Child_Y); // ...so we backtrack to the right
        }
        if (D == 2) { // This location was first visited from the left
          +pathRoute(Child_X-1, Child_Y);    
          !route_add_predecessors(Child_X-1, Child_Y); // ...so we backtrack to the left
        }
        if (D == 3) { // This location was first visited from the above
          +pathRoute(Child_X, Child_Y-1);
          !route_add_predecessors(Child_X, Child_Y-1);
        }
        if (D == 4) { // This location was first visited from the bellow
          +pathRoute(Child_X, Child_Y+1);
          !route_add_predecessors(Child_X, Child_Y+1); // ...so we backtrack to the bellow
        }
      } 
      // ELSE
      else {     
        .abolish(closed(_,_,_));
      }.


heuristika(MyX,MyY,X,Y,H) :- distance_X(MyX,X,Dist_X) & distance_Y(MyY,Y,Dist_Y) & H = Dist_X+Dist_Y.

f_number_left(MyX,MyY,X,Y,G,N) :- heuristika(MyX-1,MyY,X,Y,H) & N = H+G.
f_number_right(MyX,MyY,X,Y,G,N) :- heuristika(MyX+1,MyY,X,Y,H) & N = H+G.
f_number_down(MyX,MyY,X,Y,G,N) :- heuristika(MyX,MyY+1,X,Y,H) & N = H+G.
f_number_up(MyX,MyY,X,Y,G,N) :- heuristika(MyX,MyY-1,X,Y,H) & N = H+G.

get_route_array(UnvisitedArray,RouteArray) :- reverse(UnvisitedArray,RouteArray,[]).

+!find_min_F_coord
  <- for (open(X,Y,_,F,_))
     {
       if (not (minimal_F(_,_,_))) {
         -+minimal_F(F,X,Y);
       }
       else {
         if (minimal_F(_,_,_)) {
          ?minimal_F(MinF, MX, MY);
          if (F < MinF) {
            -+minimal_F(F,X,Y);
          }
        }
       }
     }.

+!fill_open(MyX,MyY,X,Y): (MyX == X) & (MyY == Y) & d(D)// ak prisiel do ciela
	<-	+closed(X,Y,D);
		!route_from_closed.  	
	   	
+!fill_open(MyX,MyY,X,Y)     // X,Y suradnice ciela
	<-	if (possibleLeft(MyX,MyY) & not closed(MyX-1,MyY,_)) {    // ak sa da ist dolava
        if (open(MyX,MyY,G,_,_)) {
			?f_number_left(MyX,MyY,X,Y,G,FF);  // vypocitaj F = H + G
          	+open(MyX-1,MyY,G,FF,1);		     
        }
        else { // This is start position, this location isnt in open
          ?f_number_left(MyX,MyY,X,Y,1,FF);
          +open(MyX-1,MyY,1,FF,1);
        }
	   	} 
	   	if (possibleRight(MyX,MyY) & not closed(MyX+1,MyY,_)) {
        if (open(MyX,MyY,G,_,_)) { 
			?f_number_right(MyX,MyY,X,Y,G,FR);  // vypocitaj F = H + G
          	+open(MyX+1,MyY,G,FR,2);		     
        }
        else { // This is start location neighbour, G=1
          ?f_number_right(MyX,MyY,X,Y,1,FR);
          +open(MyX+1,MyY,1,FR,2);
        }
	   	}
		if (possibleDown(MyX,MyY) & not closed(MyX,MyY+1,_)) {
        if (open(MyX,MyY,G,_,_)) { 
			?f_number_down(MyX,MyY,X,Y,G,FD);  // vypocitaj F = H + G
          	+open(MyX,MyY+1,G,FD,3);		     
        }
        else { // This is start location neighbour, G=1
          ?f_number_down(MyX,MyY,X,Y,1,FD);
          +open(MyX,MyY+1,1,FD,3);
        }
		}
		if (possibleUp(MyX,MyY) & not closed(MyX,MyY-1,_)) {
        if (open(MyX,MyY,G,_,_)) { 
			    ?f_number_up(MyX,MyY,X,Y,G,FU);  // vypocitaj F = H + G
          +open(MyX,MyY-1,G,FU,4);		     
        }
        else { // This is start location neighbour, G=1
          ?f_number_up(MyX,MyY,X,Y,1,FU);
          +open(MyX,MyY-1,1,FU,4);
        }
		}

    // If open contains at least one element
    if (open(_,_,_,_,_)) {
      	!find_min_F_coord; //najdi open s minimalnym F
      	//?minimal_F(MinF,MF_X,MF_Y); 
      	//-minimal_F(MinF,MF_X,MF_Y);
		
	  	if (minimal_F(_,_,_)) {
			?minimal_F(MinF,MF_X,MF_Y); 
		  	-minimal_F(MinF,MF_X,MF_Y);
		  	if (open(MF_X,MF_Y,G,MinF,D)) {   // Vlez do open			  	
			  	+closed(MF_X,MF_Y,D);				// uloz do closed 
			  	-open(MF_X,MF_Y,G,MinF,D);				// vymaz open
			  	-+d(D);
			  	!fill_open(MF_X,MF_Y,X,Y);			// zavolaj fill open so suradnicami co sa ulozily do closed teda ide sa na dalsie policko
      		}
      	}
	}.

+!calculate_plan([X,Y]): pos(MyX,MyY)
	<- .abolish(pathRoute(_,_)); !fill_open(MyX,MyY,X,Y); .abolish(closed(_,_,_)); .abolish(open(_,_,_,_,_)).
		

// i am at the same position as slow agent - i must wait till he makes another move
+!go([X,Y]): state(following) & moves(M) & (M>0) & pos(MyX,MyY) & (MyX == X) & (MyY == Y)
	<- -pathRoute(X,Y);-+moves(M-1);do(skip).
	
+!go([X,Y]): state(pickingResource) & 
	pos(MyX,MyY) & (MyX == X) & (MyY == Y) & moves(M) & (M>0) & maxMoves(MM)
	<-  //?follower(aFast);
			if(ally(X,Y) & (M==MM)){
				if(resourceType(gold) & gold(X,Y)){
					-goldList(X,Y);
					-+moves(0);
					
					-actualResource([X,Y]);
					-pathRoute(X,Y);
					do(pick);
					!checkCapacity;
				}
				else{
					if(resourceType(wood) & wood(X,Y)){
						-woodList(X,Y);
						-+moves(0);
					
						-actualResource([X,Y]);
						-pathRoute(X,Y);
						do(pick);
						!checkCapacity;
					}
					else{
						if(goldList(X,Y)) {
							-goldList(X,Y);
							-+moves(0);
							
							-actualResource([X,Y]);
							-pathRoute(X,Y);
							!checkCapacity;
						}
						if(woodList(X,Y)) {
							-woodList(X,Y);
							-+moves(0);
							
							-actualResource([X,Y]);
							-pathRoute(X,Y);
							!checkCapacity;
						}
						
						// send for fast to help me pick resource
						//.send(aFast, tell, help);
//						.print("skipujem v druhom else");
						-+moves(M-1);-pathRoute(X,Y);
						do(skip);
					}
				}
			}
			else{
				// send for fast to help me pick resource
				//.send(aFast, tell, help);
//				.print("skipujem v prvom else");
				-+moves(M-1);-pathRoute(X,Y);
				do(skip);
			}.
		
		
			
+!go([X,Y]): state(goToDepot) 
	& pos(MyX,MyY) & (MyX == X) & (MyY == Y) & moves(M) & maxMoves(MM) & (M==MM)
	<-  -+moves(0); 
		 do(drop);

		.findall([GX,GY], goldList(GX,GY), GoldArray); 
		.length(GoldArray,GL);
		.findall([WX,WY], woodList(WX,WY), WoodArray); 
		.length(WoodArray,WL);

		// if I know any other gold position I go there
		if(GL > 0){
			-+state(pickingResource);
			-+resourceType(gold);
			!actGold(GoldArray);
		}		
		else {// if I know any other wood position I go there
			if(WL > 0){
				-+state(pickingResource);
				-+resourceType(wood);
				!actGold(WoodArray);
			}
		}
		// if I don't know any other resource position I follow slow agent
		if(GL == 0 & WL == 0){
			-+state(following);			
		}.
	
// if I haven't go enough moves to drop resource I wait
+!go([X,Y]): state(goToDepot) 
	& pos(MyX,MyY) & (MyX == X) & (MyY == Y) & moves(M) & maxMoves(MM) & (M>0) & (M<MM) 
	<- -+moves(M-1);-pathRoute(X,Y); do(skip).
	

+!go([X,Y]): pos(MyX,MyY) & possibleLeft(MyX,MyY) & (MyX > X) & moves(M) & (M>0)
	<-  -+moves(M-1); -pathRoute(X,Y); ?moves(MM); if (MM >= 0) {do(left)}.
+!go([X,Y]): pos(MyX,MyY) & possibleRight(MyX,MyY)& (MyX < X) & moves(M) & (M>0)
	<-  -+moves(M-1); -pathRoute(X,Y); ?moves(MM); if (MM >= 0) {do(right)}.
+!go([X,Y]): pos(MyX,MyY) & possibleDown(MyX,MyY) & (MyY < Y) & moves(M) & (M>0)
	<-  -+moves(M-1); -pathRoute(X,Y); ?moves(MM); if (MM >= 0) {do(down)}.
+!go([X,Y]): pos(MyX,MyY) & possibleUp(MyX,MyY) & (MyY > Y) & moves(M) & (M>0)
	<-  -+moves(M-1); -pathRoute(X,Y); ?moves(MM); if (MM >= 0) {do(up)}.
		
+!go([X,Y]): moves(M) & (M>0) <- -+moves(M-1); -pathRoute(X,Y);do(skip).
+!go([X,Y]).


+!visit([F|T]): moves(M) & (M>0) <-	!go(F).
+!visit([F]): moves(M) & (M>0) <-!go(F). 
+!visit([]): moves(M) & (M>0) <- -+moves(M-1); do(skip).

+!visit_place([F|T]) <- 
      !find_obstacles;
      !set_dest_place(F);

      if (destination(Dst_X, Dst_Y) & known_obstacle(Obs_X, Obs_Y) & Dst_X == Obs_X & Dst_Y == Obs_Y) {
        .abolish(pathRoute(_,_));
        !visit([]);
      }
      else {
        //.print("Zacatek visit place");
        if (not pathRoute(_,_) | new_obstacle(_)) {
          -new_obstacle(true);
          !calculate_plan(F);
        }
        // There is no route leading to our goal
        if (not pathRoute(_,_)) {
          ?destination(X,Y);
          //.print("There is still no plan to ", X, " ",Y);
          !visit([[X,Y]]);
        } else {
          .findall([A,B], pathRoute(A,B), WayToGo);
          //.print("pathRoute: ", WayToGo);
		  !visit(WayToGo);
        }
      }.  

+!visit_place([F]) <- 
      !find_obstacles;
      !set_dest_place(F);

      ?destination(Dst_X, Dst_Y);
      if (not known_obstacle(Dst_X, Dst_Y)) {
        if (not pathRoute(_,_) | new_obstacle(_)) {
          -new_obstacle(true);
          !calculate_plan(F);
        }
        // There is no route leading to our goal
        if (not pathRoute(_,_)) {
          ?destination(X,Y);
//          .print("There is still no plan to ", X, " ",Y);
          !visit([[X,Y]]);
        } else {
          .findall([A,B], pathRoute(A,B), WayToGo);
//          .print("pathRoute: ", WayToGo);
		      !visit(WayToGo);
        }  
      } else {
        .abolish(pathRoute(_,_));
        -new_obstacle(true);
        !visit([]);
      }.

+!visit_place([]): moves(M) & (M>0) <- -+moves(M-1); do(skip).

// **************************************************** //
// ------------- Main actions during round ------------ //
// **************************************************** //
+!checkCapacity: resourceType(gold) & carrying_capacity(Cc) & carrying_gold(Cg) & Cg > 0 & Cg < Cc
	<-  .findall([GX,GY], goldList(GX,GY), GoldArray); 
		.length(GoldArray,GL);
		// if I know any other gold position I go there
		if(GL > 0){
			-+state(pickingResource);
			-+resourceType(gold);
			!actGold(GoldArray);
		}
		else {
			if (Cg == 0) {
				-+state(following);
			}
			else {
				-+state(goToDepot);
			}
			
//			?depot(X,Y); 
//		    !visit_place([[X,Y]]);
		    ?follower(F);
			.send(F, untell, help);
//		    .send(aFast, tell, pickerDes(X,Y));
		}.

//+!checkGoldCapacity: true
//	<- //?depot(X,Y); 
//	    //!visit_place([[X,Y]]);
//	    -+state(goToDepot);
//	    ?follower(F);
//		.send(F, untell, help).
//	    //.send(aFast, tell, pickerDes(X,Y)).


+!checkCapacity: resourceType(wood) & carrying_capacity(Cc) & carrying_wood(Cw) & Cw > 0 & Cw < Cc
	<-  .findall([WX,WY], woodList(WX,WY), WoodArray); 
		.length(WoodArray,WL);
		// if I know any other wood position I go there
		if(WL > 0){
			-+state(pickingResource);
			-+resourceType(wood);
			!actGold(WoodArray);
		}
		else {
			if (Cw == 0) {
				-+state(following);
			}
			else {
				-+state(goToDepot);
			}
//			?depot(X,Y); 
//		    !visit_place([[X,Y]]);
		    ?follower(F);
			.send(F, untell, help);
		    //.send(aFast, tell, pickerDes(X,Y));
		}.
+!checkCapacity: true
	<- //?depot(X,Y); 
	    //!visit_place([[X,Y]]);
	    -+state(goToDepot);
	    ?follower(F);
		.send(F, untell, help).
	    //.send(aFast, tell, pickerDes(X,Y)).

+!turn: state(following) & moves(M) & (M>0) & goTo(X,Y)
	<- 	if (not semaphore(SEM)) {
        +semaphore(1);	
        !visit_place([[X,Y]]);
       -semaphore(1);
      }
		  !turn. 

// following but i don't know where to go (first round)
+!turn: state(following) & moves(M) & (M>0)
	<- if (not semaphore(SEM)) {
       +semaphore(1);
       -+moves(M-1); 
       do(skip);
      -semaphore(1);
     }
     !turn.
     

+!turn: state(pickingResource) & moves(M) & (M>0) & actualResource([X,Y])
	<- if (not semaphore(SEM)) {
       +semaphore(1);	
//       if (resourceType(wood) & wood(X,Y)) {
//       	.abolish(wood(X,Y));
//       }
//       else {
//       	if (resourceType(gold) & gold(X,Y)) {
//       		.abolish(gold(X,Y));
//       	}
//       }
       !visit_place([[X,Y]]);
       ?follower(F);
   	   .send(F, tell, help);
	   .send(F, tell, pickerDes(X,Y));
       -semaphore(1);
     }
		 !turn.
 

+!turn: state(goToDepot) & moves(M) & (M>0)
	<- 	if (not semaphore(SEM)) {
	        +semaphore(1);
//	        !checkCapacity;
	        ?depot(X,Y); 
		    !visit_place([[X,Y]]);
	        -semaphore(1);
	    }
		!turn.
		
// this plan is used only when agent choose his actions in wrong order 
// for example he removes his actual state but doesnt add new state 
+!turn: moves(M) & (M>0) <- !turn.
+!turn.

+step(0) <- !choosePicker;-+moves(2);!turn.
+step(_) <- -+moves(2);!turn.

//+step(X) <- do(skip);do(skip).
